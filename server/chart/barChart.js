var pool = require('../database');
let util = require('../global/utilit');

exports.getBarChartData = async function (req, res) {
    try {

        let body = req.body;
        let empId = body.empid;
        let firstDate = body.start;
        let firstDateOfServer = new Date(new Date(firstDate).toLocaleDateString()).getTime();
        let timediff = firstDateOfServer - firstDate;
        let endDate = body.end;
        // if(endDate<firstDate+86400000){
        //     endDate = firstDate;
        // }
        
        let resarrayOfDay = {};
        let callbackArray = [];
        let callbackquery = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) order by date asc", [empId, firstDate, endDate]);
        for (let i = 0; i < 7; i++) {
            
            resarrayOfDay[i] = await Promise.all([
                pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and DAYOFWEEK(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId,i+1]),
                pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=2 and DAYOFWEEK(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId,i+1]),
                pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=3 and DAYOFWEEK(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId, i+1]),
                pool.query('select * from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and logbook.number not in (select logbook.number from logbook where employee_id=? and date<?) and DAYOFWEEK(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId, empId, firstDate, i+1]),
                pool.query('select count(*) as totalCount from logbook where (date BETWEEN ? and ?) and employee_id=? and DAYOFWEEK(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId, i+1])
            ]);
            // let callbackquery1 = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) and DAYOFWEEK(FROM_UNIXTIME(logbook.date/1000))=? order by date asc", [empId, firstDate, endDate, i+1]);
           
            // let callbackdelay = util.getCallBackDelay(callbackquery1);
           
            // callbackArray[i] = callbackquery1;
        }
        
        let chartDataOfDay = [];
        let daysofweek = ["SUN", "MON", "TUE", "WED", "THD", "FRD", "SAT"];
        let nTotal;
        for (let i = 0; i < 7; i++) {
            nTotal = resarrayOfDay[i][4][0].totalCount ? resarrayOfDay[i][4][0].totalCount : 1;
            chartDataOfDay.push({
                label: daysofweek[i],
                incomingCalls: resarrayOfDay[i][0][0]?resarrayOfDay[i][0][0].count:0,
                incomingCallSum: resarrayOfDay[i][0][0]?resarrayOfDay[i][0][0].sum:0,
                incomingCallAvg: resarrayOfDay[i][0][0]?resarrayOfDay[i][0][0].average:0,

                outCalls: resarrayOfDay[i][1][0]?resarrayOfDay[i][1][0].count:0,
                outCallSum: resarrayOfDay[i][1][0]?resarrayOfDay[i][1][0].sum:0,
                outCallAvg: resarrayOfDay[i][1][0]?resarrayOfDay[i][1][0].average:0,

                missedCalls: resarrayOfDay[i][2][0]?resarrayOfDay[i][2][0].count:0,
                missedCallPercent: util.getMissedCallPercentage(resarrayOfDay[i][0][0], resarrayOfDay[i][2][0]),

                newCalls: util.getNewCalls(resarrayOfDay[i][3]),
                newCallPercent: util.getNewCallsPercentage(util.getNewCalls(resarrayOfDay[i][3]), resarrayOfDay[i][0][0]?resarrayOfDay[i][0][0].count:undefined),
                // callbackDelay: callbackArray[i]
            });
        }
        res.send({
            success: true,
            chartDataOfDay,
            callbackquery: callbackquery
        });

    } catch (e) {
        res.json({
            success: false,
            message: e.message
        })
    }
}

exports.getHourlyBarChartData = async function (req, res) {
    try {

        let body = req.body;
        let empId = body.empid;
        let firstDate = body.start;
        let timezonediff = body.timezonediff;
        let endDate = body.end;

        let qEmpInfo = await pool.query("select HOUR(start_work_time) as start_hour, MINUTE(start_work_time) as start_min, HOUR(end_work_time) as end_hour, MINUTE(end_work_time) as end_min from employee where id=?", [empId]);
        empInfo = qEmpInfo[0];
        let start_hour = Math.floor(empInfo.start_hour);
        let end_hour = Math.floor(empInfo.end_hour);
        let start_min = Math.floor(empInfo.start_min);
        let end_min = Math.floor(empInfo.end_min);
        if(end_hour == 0){
            end_hour = 23;
        }
        
        let resarrayOfHourly = [];
        let callbackArray = [];
        let callbackquery1;

        // ============== update ================
        start_hour = 0;
        end_hour = 24;
        end_min = 0;
        start_min = 0;

        let unix_time = 0;
        let hourdiff = Math.floor(timezonediff/60);
        let callbackquery = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) order by date asc", [empId, firstDate, endDate]);
        for (let i = start_hour; i <end_hour; i++) {
            // if(i != start_hour){
                unix_time = (i + hourdiff);
                if(unix_time<0){
                    unix_time = 24 + unix_time;
                }
                resarrayOfHourly[i] = await Promise.all([
                    pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and HOUR(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId, unix_time]),
                    pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?)and employee_id=? and type=2 and HOUR(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId,unix_time]),
                    pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=3 and HOUR(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId, unix_time]),
                    pool.query('select * from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and logbook.number not in (select logbook.number from logbook where employee_id=? and date<?) and HOUR(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId, empId, firstDate,unix_time]),
                    pool.query('select count(*) as totalCount from logbook where (date BETWEEN ? and ?) and employee_id=? and HOUR(FROM_UNIXTIME(logbook.date/1000))=?', [firstDate, endDate, empId, unix_time])
                   ]);
        
                //    callbackquery1 = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) and HOUR(FROM_UNIXTIME(logbook.date/1000))=? order by date asc", [empId, firstDate, endDate, unix_time]);
                  // let callbackdelay = util.getCallBackDelay(callbackquery1);
                    
                    // callbackArray[i] = callbackquery1;
            // }else{
                // resarrayOfHourly[i] = await Promise.all([
                //     pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))>=?', [firstDate, endDate, empId, i, start_min]),
                //     pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?)and employee_id=? and type=2 and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))>=?', [firstDate, endDate, empId,i, start_min]),
                //     pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=3 and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))>=?', [firstDate, endDate, empId, i, start_min]),
                //     pool.query('select * from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and logbook.number not in (select logbook.number from logbook where employee_id=? and date<?) and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))>=?', [firstDate, endDate, empId, empId, firstDate,i, start_min]),
                //     pool.query('select count(*) as totalCount from logbook where (date BETWEEN ? and ?) and employee_id=? and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))>=?', [firstDate, endDate, empId, i, start_min])
                //    ]);
        
                //    callbackquery1 = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))>=? order by date asc", [empId, firstDate, endDate, i, start_min]);
                //   // let callbackdelay = util.getCallBackDelay(callbackquery1);
                    
                //     callbackArray[i] = callbackquery1;
            // }

        }

        // if(end_min>0){
        //     resarrayOfHourly[end_hour] = await Promise.all([
        //         pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))<=?', [firstDate, endDate, empId, end_hour, end_min]),
        //         pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?)and employee_id=? and type=2 and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))<=?', [firstDate, endDate, empId, end_hour, end_min]),
        //         pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=3 and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))<=?', [firstDate, endDate, empId, end_hour, end_min]),
        //         pool.query('select * from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and logbook.number not in (select logbook.number from logbook where employee_id=? and date<?) and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))<=?', [firstDate, endDate, empId, empId, firstDate,end_hour, end_min]),
        //         pool.query('select count(*) as totalCount from logbook where (date BETWEEN ? and ?) and employee_id=? and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))<=?', [firstDate, endDate, empId, end_hour, end_min])
        //        ]);
    
        //        callbackquery1 = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) and HOUR(FROM_UNIXTIME(logbook.date/1000))=? and MINUTE(FROM_UNIXTIME(logbook.date/1000))<=? order by date asc", [empId, firstDate, endDate, end_hour, end_min]);
        //       // let callbackdelay = util.getCallBackDelay(callbackquery1);
                
        //         callbackArray[end_hour] = callbackquery1;
        // }

      //  let resarrayOfHourly = await Promise.all(queries);
        let chartDataOfHourly = [];
        let h1, h2;
      

        // ================ update ======================
        // hourdiff = 0;
        let i;
        for (let j = 0; j < 24; j++) {
            i = j;
            // if(i<0){
            //     i = i + 24;
            // }
            
            nTotal = resarrayOfHourly[i]&&resarrayOfHourly[i][4][0]&&resarrayOfHourly[i][4][0].totalCount ? resarrayOfHourly[i][4][0].totalCount : 1;

            h1 = j > 12 ? (j - 12) + 'PM' : j + 'AM';
            h2 = (j + 1) > 12 ? (j - 11) + 'PM' : (j + 1) + 'AM';
            chartDataOfHourly.push({ 
                label: h1 + " - " + h2, 
                incomingCalls: resarrayOfHourly[i]&&resarrayOfHourly[i][0][0]?resarrayOfHourly[i][0][0].count:0,
                incomingCallSum: resarrayOfHourly[i]&&resarrayOfHourly[i][0][0]?resarrayOfHourly[i][0][0].sum:0,
                incomingCallAvg: resarrayOfHourly[i]&&resarrayOfHourly[i][0][0]?resarrayOfHourly[i][0][0].average:0,

                outCalls: resarrayOfHourly[i]&&resarrayOfHourly[i][1][0]?resarrayOfHourly[i][1][0].count:0,
                outCallSum: resarrayOfHourly[i]&&resarrayOfHourly[i][1][0]?resarrayOfHourly[i][1][0].sum:0,
                outCallAvg: resarrayOfHourly[i]&&resarrayOfHourly[i][1][0]?resarrayOfHourly[i][1][0].average:0,

                missedCalls: resarrayOfHourly[i]&&resarrayOfHourly[i][2][0]?resarrayOfHourly[i][2][0].count:0,
                missedCallPercent: resarrayOfHourly[i]?util.getMissedCallPercentage(resarrayOfHourly[i][0][0], resarrayOfHourly[i][2][0]):0,

                newCalls: resarrayOfHourly[i]?util.getNewCalls(resarrayOfHourly[i][3]):0,
                newCallPercent: resarrayOfHourly[i]?util.getNewCallsPercentage(util.getNewCalls(resarrayOfHourly[i][3]), resarrayOfHourly[i]&&resarrayOfHourly[i][0][0]?resarrayOfHourly[i][0][0].count:undefined):0,
                // callbackDelay: callbackArray[i]?callbackArray[i]:[]
            });
        }

        res.send({
            success: true,
            chartDataOfHourly,
            callbackquery
        });

    } catch (e) {
        res.json({
            success: false,
            message: e.message
        })
    }
}