/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100132
Source Host           : localhost:3306
Source Database       : caminho

Target Server Type    : MYSQL
Target Server Version : 100132
File Encoding         : 65001

Date: 2018-08-10 13:37:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout` enum('frown','meh','smile') COLLATE utf8_unicode_ci DEFAULT NULL,
  `articles` enum('frown','meh','smile') COLLATE utf8_unicode_ci DEFAULT NULL,
  `books` enum('frown','meh','smile') COLLATE utf8_unicode_ci DEFAULT NULL,
  `action` enum('frown','meh','smile') COLLATE utf8_unicode_ci DEFAULT NULL,
  `socialize` enum('frown','meh','smile') COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
