var pool = require('../database');
let util = require('../global/utilit');
exports.getLineChartData = async function (req, res) {
    try {
        let body = req.body;
        let startofclient = body.start;

        let start = new Date(new Date(startofclient).toUTCString()).getTime();
        let timediff = startofclient - start;
        timediff = 0;
        let endofclient = body.end;
        let end = new Date(new Date(endofclient).toUTCString()).getTime();
        let empid = body.empid;
        let period = body.period;

        let chartData = {

        };


        let step = Math.floor(period);

        let resarray; // temp variable

        let missedArray = [];
        let missedPercentArray = [];

        let newcallArray = [];
        let newCallPercentArray = [];

        let incomingArray = [];
        let incomingCallDurationSumArray = [];
        let incomingCallDurationAvgArray = [];

        let outgoingArray = [];
        let outgoingCallDurationSumArray = [];
        let outgoingCallDurationAvgArray = [];
        let dateArray = [];

        let callbackArray = [];

        let totalCount;

        console.log('totalquery start', new Date().getTime());

        let totalQuery = await pool.query('select count(*) as totalCount from logbook where (date BETWEEN ? and ?) and employee_id=?', [start, end, empid]);
        console.log('totalquery end', new Date().getTime());
        totalCount = totalQuery[0] ? totalQuery[0].totalCount : 0;

        let date_format_opt = { year: 'numeric', month: 'short', day: 'numeric' };
        let date_locale_opt = "en-US";

        if (totalCount === 0) {
            res.send({
                success: false,
                message: "No Data"
            });
            return;
        }
        console.log('totalValueArray start', new Date().getTime());
        let totalValueArray = await Promise.all([
            pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1', [start, end, empid]),
            pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?)and employee_id=? and type=2', [start, end, empid]),
            pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=3', [start, end, empid]),
            pool.query('select * from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and logbook.number not in (select logbook.number from logbook where employee_id=? and date<?) order by logbook.date asc', [start, end, empid, empid,start]),
            // pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=5', [start, end, empid]),
        ]);

        console.log('totalValueArray end', new Date().getTime());

        let callbackquery = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) order by date asc", [empid, start, end]);
        //let callbackdelay_total = util.getCallBackDelay(callbackquery);
        
        console.log('resarray start', new Date().getTime());

        for (let time = start; time <=end; time += step) {

            console.log('resarray promise start', new Date().getTime());
            resarray = await Promise.all([
                pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1', [time, time + step, empid]),
                pool.query('select count(type) as count,sum(duration) as sum,avg(duration) as average from logbook where (date BETWEEN ? and ?) and employee_id=? and type=2', [time, time + step, empid]),
                pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=3', [time, time + step, empid]),
                pool.query('select * from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and logbook.number not in (select logbook.number from logbook where employee_id=? and date<?) order by logbook.date asc', [time, time + step, empid, empid, time]),
                // pool.query('select count(type) as count,sum(duration) as sum from logbook where (date BETWEEN ? and ?) and employee_id=? and type=1 and logbook.number not in (select logbook.number from logbook where employee_id=? and date<?)', [time, time + step, empid, empid, time+step]),
                // pool.query('select count(*) as totalCount from logbook where (date BETWEEN ? and ?) and employee_id=?', [time, time + step, empid])
            ]);
            console.log('resarray promise end', new Date().getTime());

            // let callbackquery1 = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) order by date asc", [empid, time, time + step]);
           // let callbackdelay = util.getCallBackDelay(callbackquery1);
            
            // let nTotal = (resarray[4][0] && resarray[4][0].totalCount) ? resarray[4][0].totalCount : 1;

            console.log('resarray raw data start', new Date().getTime());
            dateArray.push(time + timediff);
            incomingArray.push((resarray[0][0] && resarray[0][0].count) ? Math.round(resarray[0][0].count) : 0);
            incomingCallDurationSumArray.push((resarray[0][0] && resarray[0][0].sum) ? Math.round(resarray[0][0].sum) : 0);
            incomingCallDurationAvgArray.push((resarray[0][0] && resarray[0][0].average) ? Math.round(resarray[0][0].average) : 0);

            outgoingArray.push((resarray[1][0] && resarray[1][0].count) ? Math.round(resarray[1][0].count) : 0);
            outgoingCallDurationSumArray.push((resarray[1][0] && resarray[1][0].sum) ? Math.round(resarray[1][0].sum) : 0);
            outgoingCallDurationAvgArray.push((resarray[1][0] && resarray[1][0].average) ? Math.round(resarray[1][0].average) : 0);

            missedArray.push((resarray[2][0] && resarray[2][0].count) ? resarray[2][0].count : 0);
            missedPercentArray.push(util.getMissedCallPercentage(resarray[0][0], resarray[2][0]));

            newcallArray.push(util.getNewCalls(resarray[3]));
            
            newCallPercentArray.push(util.getNewCallsPercentage(util.getNewCalls(resarray[3]), (resarray[0][0] && resarray[0][0].count) ? Math.round(resarray[0][0].count) : undefined));
            // callbackArray.push(callbackquery1);
            console.log('resarray raw data  end', new Date().getTime());
        }

        console.log('resarray end', new Date().getTime());

        console.log('other start', new Date().getTime());
        chartData.dates = dateArray;
        chartData.incomingCalls = incomingArray;
        chartData.incomingCallSum = incomingCallDurationSumArray;
        chartData.incomingCallAvg = incomingCallDurationAvgArray;

        chartData.outCalls = outgoingArray;
        chartData.outCallSum = outgoingCallDurationSumArray;
        chartData.outCallAvg = outgoingCallDurationAvgArray;

        chartData.missedCalls = missedArray;
        chartData.missedCallPercent = missedPercentArray;

        chartData.newCalls = newcallArray;
        chartData.newCallPercent = newCallPercentArray;
        // chartData.callbackDelay = callbackArray;
        chartData.callbackDelay = callbackquery;

        totalData = {};
        totalData.incomingCalls = (totalValueArray[0][0] && totalValueArray[0][0].count) ? Math.round(totalValueArray[0][0].count) : 0;
        totalData.incomingCallSum = (totalValueArray[0][0] && totalValueArray[0][0].sum) ? Math.round(totalValueArray[0][0].sum) : 0;
        totalData.incomingCallAvg = (totalValueArray[0][0] && totalValueArray[0][0].average) ? Math.round(totalValueArray[0][0].average) : 0;

        totalData.outCalls = (totalValueArray[1][0] && totalValueArray[1][0].count) ? Math.round(totalValueArray[1][0].count) : 0;
        totalData.outCallSum = (totalValueArray[1][0] && totalValueArray[1][0].sum) ? Math.round(totalValueArray[1][0].sum) : 0;
        totalData.outCallAvg = (totalValueArray[1][0] && totalValueArray[1][0].average) ? Math.round(totalValueArray[1][0].average) : 0;

        totalData.missedCalls = (totalValueArray[2][0] && totalValueArray[2][0].count) ? Math.round(totalValueArray[2][0].count) : 0;
        totalData.missedCallPercent = util.getMissedCallPercentage(totalValueArray[0][0], totalValueArray[2][0]);

        totalData.newCalls = util.getNewCalls(totalValueArray[3]);
        totalData.newCallPercent = util.getNewCallsPercentage(totalData.newCalls, (totalValueArray[0][0] && totalValueArray[0][0].count) ? Math.round(totalValueArray[0][0].count) : undefined);
        totalData.callbackDelay = callbackquery;

        res.send({
            success: true,
            chartData,
            totalData,
            period: period
        });
        console.log('other end', new Date().getTime());

    } catch (err) {
        console.log('error', err.message);
        res.send({
            success: false,
            message: err.message
        })
    }
}