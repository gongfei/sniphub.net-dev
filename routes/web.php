<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

// front routes
Route::prefix('blog')->group(function (){
    Route::get('detail', 'HomeController@showBlog');
    Route::get('create', 'HomeController@createBlog')->middleware('auth');
    Route::post('save', 'HomeController@saveBlog')->middleware('auth');
    Route::post('savecontent', 'HomeController@savecontent')->middleware('auth');

    // blog view
    Route::get('editor', 'HomeController@editorPage');
    Route::get('edit', 'HomeController@editBlog')->middleware('auth');
    Route::post('search', 'HomeController@searchBlog');
    Route::get('discard', 'HomeController@discardBlog')->middleware('auth');
    Route::get('setDiscard', 'HomeController@setDiscard')->middleware('auth');

    // set published
    Route::get('setStatus', 'HomeController@setStatus')->middleware('auth');

});

Route::get('readbooks', 'HomeController@readbooks');

// become an editor page
Route::get('become_editor', 'HomeController@becomeEditor')->middleware('auth');
Route::post('requestEditor', 'HomeController@requestEditor')->middleware('auth');

/**
 * ========================= Admin Panel ===========================
 */
Route::prefix('backend')->group(function (){
    // users routes
    Route::get('/dashboard', 'AdminController@dashboard');
    Route::get('/user/view', 'AdminController@user_view');
    Route::get('/user/edit', 'AdminController@user_edit');
    Route::get('/user/delete', 'AdminController@user_delete');
    Route::get('/user/promote_editor', 'AdminController@promote_editor');

    // setting route
    Route::get('/setting', 'AdminController@setting');

    //filter routes
    Route::get('filter/edit', 'AdminController@filter_edit');
    Route::get('filter/delete', 'AdminController@filter_delete');
    Route::get('filter/create', 'AdminController@filter_create');
    Route::get('filter/save', 'AdminController@filter_save');
});

/**
 *  ======================== editor's urls ===========================
 *
 */
// profile
Route::prefix('profile')->group(function (){
    Route::post('save', 'EditorController@saveProfile');
});

/**
 * ======================== Common urls =============================
 */
// socialize
Route::get('socialize', 'HomeController@socialize');
//action
Route::get('action', 'HomeController@action');

// feedback page
Route::get('give_feedback', 'HomeController@feedback');
Route::post('post_feedback', 'HomeController@postFeedback')->middleware('auth');

// Api
Route::prefix('api')->group(function (){
    Route::get('getFilters', 'ApiController@getFilters');
});

/**
 * ======================== Comments ================================
 */
Route::post('comment/post', 'HomeController@post_comment')->middleware('auth');

// ====================================== bottom menus =========================================
Route::get('about_us', 'HomeController@about_us');
Route::get('vision', 'HomeController@vision');
Route::get('mission', 'HomeController@mission');
Route::get('values', 'HomeController@values');


// ====================================== Exception handler ======================================
Route::get('postTooLargeExcept', 'HomeController@postTooLargeExcept');