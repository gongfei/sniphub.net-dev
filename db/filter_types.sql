/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100132
Source Host           : localhost:3306
Source Database       : caminho

Target Server Type    : MYSQL
Target Server Version : 100132
File Encoding         : 65001

Date: 2018-08-12 02:55:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for filter_types
-- ----------------------------
DROP TABLE IF EXISTS `filter_types`;
CREATE TABLE `filter_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('article','book','action','socialize') COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of filter_types
-- ----------------------------
INSERT INTO `filter_types` VALUES ('1', 'article', 'political_science', 'Ciência Política');
INSERT INTO `filter_types` VALUES ('2', 'article', 'psychology', 'psicologia');
INSERT INTO `filter_types` VALUES ('3', 'article', 'economy', 'economia');
INSERT INTO `filter_types` VALUES ('4', 'article', 'history', 'história');
INSERT INTO `filter_types` VALUES ('5', 'action', 'planning', 'planejamento');
INSERT INTO `filter_types` VALUES ('6', 'action', 'tools', 'Ferramentas');
INSERT INTO `filter_types` VALUES ('8', 'article', 'lessons', 'lições');
INSERT INTO `filter_types` VALUES ('9', 'book', 'science', 'Ciência');
INSERT INTO `filter_types` VALUES ('10', 'book', 'physical', 'fisica');
INSERT INTO `filter_types` VALUES ('15', 'socialize', 'facebook_groups', 'grupos no facebook');
INSERT INTO `filter_types` VALUES ('16', 'socialize', 'meetup_groups', 'grupos de encontro');
INSERT INTO `filter_types` VALUES ('17', 'socialize', 'other blogs', 'outros blogs');
