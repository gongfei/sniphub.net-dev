exports.getMonday = function (d) {
    d = new Date(d);
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
    return new Date(d.setDate(diff));
}

exports.getTimeStamp = function (timestamp) {
    let hour = Math.floor(timestamp / 3600);
    hour = hour === 0 ? "00" : (hour > 0 && hour < 10) ? "0" + hour : hour;
    let min = Math.floor((timestamp % 3600) / 60);
    min = min === 0 ? "00" : (min > 0 && min < 10) ? "0" + min : min;
    let sec = (Math.round(timestamp) % 3600) % 60;
    sec = sec === 0 ? "00" : (sec > 0 && sec < 10) ? "0" + sec : sec;
    return "" + hour + ":" + min + ":" + sec;
}
exports.formatAMPM = function (date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = (hours >= 10 ? hours : ("0" + hours)) + ':' + minutes + ' ' + ampm;
    return strTime;
}

exports.formatAMPM_HOUR = function (date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    var strTime = (hours >= 10 ? hours : ("0" + hours)) + ' ' + ampm;
    return strTime;
}

exports.formatDate = function (timestamp, date_locale_opt, date_format_opt) {
    let d = new Date(timestamp);
    return d.toLocaleDateString(date_locale_opt, date_format_opt);
}

exports.max = function (a, b) {
    return a > b ? a : b;
}

exports.min = function (a, b) {
    return a > b ? b : a;
}


exports.getCallBackDelay = function (callbackquery) {
    let firsttime = 0;
    let timeVal = 0;

    let dateVal_first_missed = ''
    let dateVal_next_to_first_missed = '';

    let callbackdelay = 0;

    for (let i = 0; i < callbackquery.length; i++) {
        if (callbackquery[i].type == 3) {
            if(firsttime===0){
                timeVal = new Date(callbackquery[i].date).getHours();
                if (timeVal >= 9 && timeVal <= 21) {
                    firsttime = callbackquery[i].date;
                    dateVal_first_missed = new Date(firsttime).toDateString();
                }
            }
           
        }
        if (callbackquery[i].type == 2 || callbackquery[i].type == 1) {
            if (firsttime !== 0) {

                timeVal = new Date(callbackquery[i].date).getHours();
                dateVal_next_to_first_missed = new Date(callbackquery[i].date).toDateString();
                if (timeVal >= 9 && timeVal <= 21 && dateVal_first_missed == dateVal_next_to_first_missed) {
                    callbackdelay += Math.floor(callbackquery[i].date) - Math.floor(firsttime);
                }
                firsttime = 0;
            }
        }
    }

    return callbackdelay;
}

exports.makeToken = function(){

  let text = "";
  let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 15; i++){
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;

}

// get new calls
exports.getNewCalls = function(arr){
    let count = 0;
    let i;
  
    let prevItems;
    for(i = 0; i<arr.length; i++){
        prevItems = [];
        prevItems = arr.filter(function(el){
            return el.number == arr[i].number && el.date<arr[i].date
        });
        if(prevItems.length == 0){
            count++;
        }
    }
    return count;
}

exports.getNewCallsArray = function(arr){
    let res = [];
    let i;
  
    let prevItems;
    for(i = 0; i<arr.length; i++){
        prevItems = [];
        prevItems = arr.filter(function(el){
            return el.number == arr[i].number && el.date<arr[i].date
        });
        if(prevItems.length == 0){
            res.push(arr[i]);
        }
    }
    return res;
}

// get new calls percentage
exports.getNewCallsPercentage = function(nNewCalls, nIncoming){
    if(nIncoming == undefined){
        return 0;
    }
    return Math.floor((nNewCalls/nIncoming) * 10000)/100;

}

// GET missedcall percentage
exports.getMissedCallPercentage = function(nIncoming, nMissed){
  
    let percent = 0;
    if(!nIncoming || !nMissed){
        return percent;
    }
    if(nIncoming.count !== undefined && nMissed.count !== undefined){
        percent = nMissed.count/(nIncoming.count + nMissed.count)*100;
        if(percent != Infinity){
            return percent;
        }
        percent = 0;
    }
    return percent;
}


exports.getPeriodicCount = function(arr, step){
    
    if(arr.length==0){
        return 0;
    }
    let count = 1;
    
    let firstItem = arr[0];
    let len = arr.length;
    
    while(true){
        if(firstItem.date>arr[len - 1].date){
            break;
        }
        let tmpArr = arr.filter(a=>{
            return (a.date>=firstItem.date+step);
        });
    
        if(tmpArr.length==0){
            break;
        }
        firstItem = tmpArr[0];
        count++;
    }
    return count;
}