let pool = require('../database')
let util = require('../global/utilit');
//

exports.getsocial = async function (req, res) {

    let fromdate = 000000000;
    let todate = new Date();
    todate.setUTCHours(23, 59, 59, 999);
    todate = Math.round(todate.getTime())

    if (req.body.fromdate) {
        fromdate = new Date((req.body.fromdate));
        //fromdate.setUTCHours(0, 0, 0, 0);
        fromdate = Math.round(fromdate.getTime());
    }
    if (req.body.todate) {
        todate = new Date((req.body.todate));

        //todate.setUTCHours(23, 59, 59, 999);
        todate = todate.setDate(todate.getDate() + 1);
        todate = new Date(todate).toDateString();
        todate = Math.round(new Date(todate).getTime());
    }

    let expression = req.body.type;


    switch (expression) {
        case 'topCallers':
            let gettopcallers = await topCallers(req.body, fromdate, todate);
            res.send({
                success: true,
                data: gettopcallers
            });
            break;
        case 'totalDurations':
            let gettotalduration = await totalduration(req.body, fromdate, todate, req.body.limitFrom);
            res.send({
                success: true,
                data: gettotalduration
            });
            break;
        case 'forgotenCallers':
            let getIgnore = await forgotten(req.body, fromdate, todate, req.body.limitFrom);
            res.send({
                success: true,
                data: getIgnore
            });
            break;
        case 'averageDurations':
            let getaverages = await Averages(req.body, fromdate, todate, req.body.limitFrom);
            res.send({
                success: true,
                data: getaverages
            });
            break;
        case 'pastFriends':
            let pastfriend = await pastFriend(req.body, fromdate, todate, req.body.limitFrom);
            res.send({
                success: true,
                data: pastfriend
            });
            break;
        case 'ignoreres':
            let ignoreres = await ignore(req.body, fromdate, todate, req.body.limitFrom);
            res.send({
                success: true,
                data: ignoreres
            });
            break;
        case 'friendliest':
            let friendliest = await friendList(req.body, fromdate, todate, req.body.limitFrom);
            res.send({
                success: true,
                data: friendliest
            });
            break;
        case "weeklydashboard":
            {
                let weeklydashboard = await weekdashboard(req.body);
                res.send({
                    success: true,
                    data: weeklydashboard
                });
                break;
            }
        case "specialCalls":
            {
                let special = await specialcalls(req.body, fromdate, todate, req.body.limitFrom);
                res.send({
                    success: true,
                    data: special
                });
                break;
            }
        case "forgotenNewCalls":
            {
                let data = await forgottenNewCalls(req.body, fromdate, todate, req.body.limitFrom);
                res.send({
                    success: true,
                    data: data
                });
                break;
            }
        default: {
            res.send({
                success: false,
                message: "No exist type."
            })
            break;
        }

    }

}

// top callers
async function topCallers(body, fromdate, todate) {
    let data, data1;
    if (body.subType == 1) {
        data = await pool.query(`select logbook.number,count(*) as incomingtotalcalls
        from logbook 
        where (date BETWEEN ? and ?) and  logbook.employee_id =? and logbook.type=1
        group by number
        order by incomingtotalcalls desc limit ?, 10`, [fromdate, todate, body.employeeId, body.limitFrom]);

        for (let i = 0; i < data.length; i++) {
            data1 = await Promise.all([
                pool.query(`select count(*) as outgoingtotalcalls from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and logbook.type=2 and logbook.number=?`, [fromdate, todate, body.employeeId, data[i].number]),
                pool.query('select contact.number, contact.name, contact.photo from contact where contact.number=? and contact.employee_id=? group by number', [data[i].number, body.employeeId])
            ]);
            data[i].outgoingtotalcalls = data1[0][0] ? data1[0][0].outgoingtotalcalls : 0;
            data[i].name = data1[1][0] ? data1[1][0].name : undefined;
            data[i].photo = data1[1][0] ? data1[1][0].photo : undefined;
        }

    } else {
        data = await pool.query(`select logbook.number,count(*) as outgoingtotalcalls
        from logbook 
        where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=2)
        group by number
        order by incomingtotalcalls desc limit ?, 10`, [fromdate, todate, body.employeeId, body.limitFrom]);
        for (let i = 0; i < data.length; i++) {
            data1 = await Promise.all([
                pool.query(`select count(*) as incomingtotalcalls from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and logbook.type=1 and logbook.number=?`, [fromdate, todate, body.employeeId, data[i].number]),
                pool.query('select contact.number, contact.name, contact.photo from contact where contact.number=? and contact.employee_id=? group by number', [data[i].number, body.employeeId])
            ]);
            data[i].incomingtotalcalls = data1[0][0] ? data1[0][0].incomingtotalcalls : 0;
            data[i].name = data1[1][0] ? data1[1][0].name : undefined;
            data[i].photo = data1[1][0] ? data1[1][0].photo : undefined;
        }

    }
    return data;
}

// total duration
async function totalduration(body, fromdate, todate, limitFrom) {
    let data, data1;
    if(body.subType == 1){
        data = await pool.query(`select logbook.number,sum(logbook.duration) as incomingtotalduration
        from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=1)
        group by number
        order by incomingtotalduration desc limit ?, 10`, [fromdate, todate, body.employeeId, limitFrom]);
        
        for(let i = 0; i<data.length; i++){
            data1 = await Promise.all([
                pool.query(`select sum(logbook.duration) as outgoingtalduration from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=2) and logbook.number=?`, [fromdate, todate, body.employeeId, data[i].number]),
                pool.query('select contact.number, contact.name, contact.photo from contact where contact.number=? and contact.employee_id=? group by number', [data[i].number, body.employeeId])
            ]);
            data[i].incomingtotalduration = util.getTimeStamp(data[i].incomingtotalduration)
            data[i].outgoingtalduration = util.getTimeStamp(data1[0][0] ? data1[0][0].outgoingtalduration : 0);
            data[i].name = data1[1][0] ? data1[1][0].name : undefined;
            data[i].photo = data1[1][0] ? data1[1][0].photo : undefined;
        }
    }else{
        data = await pool.query(`select logbook.number,sum(logbook.duration) as outgoingtalduration
        from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=2)
        group by number
        order by outgoingtalduration desc limit ?, 10`, [fromdate, todate, body.employeeId, limitFrom]);
        
        for(let i = 0; i<data.length; i++){
            data1 = await Promise.all([
                pool.query(`select sum(logbook.duration) as incomingtotalduration from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=1) and logbook.number=?`, [fromdate, todate, body.employeeId, data[i].number]),
                pool.query('select contact.number, contact.name, contact.photo from contact where contact.number=? and contact.employee_id=? group by number', [data[i].number, body.employeeId])
            ]);
            data[i].outgoingtalduration = util.getTimeStamp(data[i].outgoingtalduration)
            data[i].incomingtotalduration = util.getTimeStamp(data1[0][0] ? data1[0][0].incomingtotalduration : 0);
            data[i].name = data1[1][0] ? data1[1][0].name : undefined;
            data[i].photo = data1[1][0] ? data1[1][0].photo : undefined;
        }
    }
    
    return data;
}

// special calls
async function specialcalls(body, fromdate, todate, limitFrom) {
    let data, data1;
    let special_period = Math.round(body.special_period);

    let employeeList = await pool.query("select distinct number from logbook where employee_id=? and (date BETWEEN ? and ?) Group BY number ORDER BY number desc", [body.employeeId, fromdate, todate]);
    let incomingHistories = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) and type=1 order by date asc", [body.employeeId, fromdate, todate]);
    let outgoingHistories = await pool.query("select * from logbook where employee_id=? and (date BETWEEN ? and ?) and type=2 order by date asc", [body.employeeId, fromdate, todate]);
    let i, j;
    let resarray = [];
    for(i =0; i<employeeList.length;i++){
        let res = {};
        let arrIncome = incomingHistories.filter(a=>{
            return a.number == employeeList[i].number
        });

        let arrOutcome = outgoingHistories.filter(a=>{
            return a.number == employeeList[i].number
        });

        let contactq = await pool.query("select photo, name from contact where employee_id=? and number=? group by number", [body.employeeId, employeeList[i].number]);
        res.number = employeeList[i].number;
        res.photo = contactq[0]?contactq[0].photo:undefined;
        res.name = contactq[0]?contactq[0].name:undefined;
        
        let step = 86400000 * special_period;
        
        res.income_count = util.getPeriodicCount(arrIncome, step);
        res.out_count = util.getPeriodicCount(arrOutcome, step);
        if(res.income_count == 0 && res.out_count==0){
            continue;
        }

        resarray.push(res);
    }
    for(let i=0;i<resarray.length;i++){
        for(let j=i+1; j<resarray.length;j++){
            if(resarray[i].income_count<resarray[j].income_count){
                let tmp = resarray[i];
                resarray[i]=resarray[j];
                resarray[j]=tmp;
                continue;
            }
            if(resarray[i].income_count== resarray[j].income_count){
                if(resarray[i].out_count<resarray[j].out_count){
                    let tmp = resarray[i];
                   resarray[i]=resarray[j];
                    resarray[j]=tmp;
                    continue;
                }
            }
        }
    }


    return resarray;
}

// forgotten calls
async function forgotten(body, fromdate, todate, limitFrom) {
    
    let logs = await pool.query("select logbook.date, logbook.number, logbook.type, logbook.duration from logbook where employee_id=? and (date BETWEEN ? and ?) order by date desc",
    [body.employeeId, fromdate, todate]);

    let missedLogs = [];
    let tmpLogs;
    for(let j=0; j<logs.length; j++){
        if(logs[j].type == 3){
            tmpLogs = logs.filter(x=>{
                return (x.date >logs[j].date && x.number == logs[j].number);
            });
            if(tmpLogs.length == 0){
                missedLogs.push(logs[j]);
            }
        }
    }

    let i;
    let res = [];
    let forgottenLog, contactInfo;
    for(i =0; i<missedLogs.length; i++){
        forgottenLog = await pool.query("select count(*) as count from logbook where employee_id=? and type=2 and date>? and number=?", [body.employeeId, todate, missedLogs[i].number]);
        if(forgottenLog[0]&&forgottenLog[0].count==0){
            contactInfo = await pool.query("select name from contact where employee_id=? and number=? limit 0,1", [body.employeeId, missedLogs[i].number]);
            missedLogs[i].name=contactInfo[0]?contactInfo[0].name:undefined;
            res.push(missedLogs[i]);
        }
    }
    
    return (res);
}

/** forgotten new calls */ 

async function forgottenNewCalls(body, fromdate, todate, limitFrom) {
    
    let logs = await pool.query("select logbook.date, logbook.number, logbook.type, logbook.duration from logbook where employee_id=? and (date BETWEEN ? and ?) order by date desc",
    [body.employeeId, fromdate, todate]);

    let missedLogs = [];
    let tmpLogs, pastLogs;
    for(let j=0; j<logs.length; j++){
        if(logs[j].type == 3){
            tmpLogs = logs.filter(x=>{
                return (x.date >logs[j].date && x.number == logs[j].number);
            });
            pastLogs = logs.filter(x=>{
                return (x.date <logs[j].date && x.number == logs[j].number && x.type!=3);
            });
            if(tmpLogs.length == 0 && pastLogs.length == 0){
                missedLogs.push(logs[j]);
            }
        }
    }

    let i;
    let res = [];
    let forgottenLog, contactInfo;
    for(i =0; i<missedLogs.length; i++){
        forgottenLog = await pool.query("select count(*) as count from logbook where employee_id=? and type=2 and date>? and number=?", [body.employeeId, todate, missedLogs[i].number]);
        if(forgottenLog[0]&&forgottenLog[0].count==0){
            contactInfo = await pool.query("select name from contact where employee_id=? and number=? limit 0,1", [body.employeeId, missedLogs[i].number]);
            missedLogs[i].name=contactInfo[0]?contactInfo[0].name:undefined;
            res.push(missedLogs[i]);
        }
    }
    
    return (res);
}

// past friend
async function pastFriend(body, fromdate, todate, limitFrom) {
    let today = new Date();
    let lastDate = new Date();

    lastDate = lastDate.setMonth(today.getMonth() - Math.floor(body.period));
    let data = await pool.query(`select number,type,date,count(*) as count from (select logbook.number,logbook.type,logbook.date from logbook where date<? and logbook.employee_id =? and logbook.type<>3 
        and number in (select distinct logbook.number from logbook where logbook.employee_id =? and logbook.number not in (select logbook.number from logbook where date>?) and logbook.type<>3)) AA GROUP BY number, type`, [lastDate, body.employeeId, body.employeeId, lastDate]);
    let qNumber = await pool.query(`select Distinct number from (select logbook.number,logbook.type,logbook.date from logbook where date<? and logbook.employee_id =? and logbook.type<>3 
        and number in (select distinct logbook.number from logbook where logbook.employee_id =? and logbook.number not in (select logbook.number from logbook where date>?) and logbook.type<>3)) AA GROUP BY number, type`, [lastDate, body.employeeId, body.employeeId, lastDate]);
    
    let qLog = await pool.query('select max(date) as lastDate, number from logbook group by number');
    let res = [];
    for(let i=0; i<qNumber.length; i++){
        let aData = {};
        aData.number = qNumber[i].number;
        
        let arrIncoming = data.filter(a=>{
            return a.number == qNumber[i].number && a.type == 1
        });
        let arrOutgoing = data.filter(a=>{
            return a.number == qNumber[i].number && a.type == 2
        });
        aData.incomingCall = arrIncoming&&arrIncoming.length>0?arrIncoming[0].count:0;
        aData.outgoingCall = arrOutgoing&&arrOutgoing.length>0?arrOutgoing[0].count:0;
        let qlogs = qLog.filter(a=>a.number===qNumber[i].number);     

        let lastDate = qlogs.length>0?qlogs[0].lastDate:0;
        aData.lastDate = lastDate;
        // aData.lastday = util.formatDate(lastDate);
        // aData.lasttime = util.formatAMPM(new Date(lastDate));
        let contactInfo = await pool.query("select name from contact where employee_id=? and number=? limit 0,1", [body.employeeId, qNumber[i].number]);
        aData.name = contactInfo[0]?contactInfo[0].name:undefined;
        res.push(aData);
    }
    
    let retArray = [];
    let firstItem;
    let j;
    for(i = 0;i<res.length;i++){
        for(j = 0; j<res.length; j++){
            if(res[i].incomingCall>res[j].incomingCall){
                firstItem = res[i];
                res[i] = res[j];
                res[j] = firstItem;
            }
        }
    }
    

    return res;
}

// ignore calls
async function ignore(body, fromdate, todate, limitFrom) {
    let data = await pool.query(`select logbook.number,count('*') as outgoingCall
    from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and logbook.type=2 GROUP BY number having outgoingCall>4`, [fromdate, todate, body.employeeId]);

    let data1 = await pool.query(`select logbook.number, count('*') as incomingCall
    from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and logbook.type=1 GROUP BY number`, [fromdate, todate, body.employeeId]);
    
    let data2 = [];
    let i, j;
    for(i = 0; i<data.length; i++){
        for(j = 0; j<data1.length; j++){
            if(data[i].number === data1[j].number){
                if(Math.floor(data[i].outgoingCall)>Math.floor(data1[j].incomingCall)){
                    data[i].incomingCall = data1[j].incomingCall;
                    data2.push(data[i]);
                }
            }
        }
    }
    
    for(i = 0; i<data2.length; i++){
        data2[i].factor = Math.floor(data2[i].outgoingCall)/util.max(data2[i].incomingCall, 1);
    }
    // sort
    
    let res = [];
    let curItem;
    while(res.length<data2.length){
        curItem = {factor:0};
        let maxIndex=0;
        for(i = 0; i<data2.length; i++){
            if(data2[i].factor>curItem.factor){
                curItem = data2[i];
                maxIndex = i;
            }
        }
        data2[maxIndex].factor = 0;
        let contactInfo = await pool.query("select name from contact where employee_id=? and number=? limit 0,1", [body.employeeId, curItem.number]);
        curItem.name = contactInfo[0]?contactInfo[0].name:undefined;
        res.push(curItem);
    }
    return (res);
}

// friendlist

async function friendList(body, fromdate, todate, limitFrom) {
    let data = await pool.query(`select logbook.number,count('*') as incomingCall
    from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and logbook.type=1 GROUP BY number having incomingCall>4`, [fromdate, todate, body.employeeId]);

    let data1 = await pool.query(`select logbook.number, count('*') as outgoingCall
    from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and logbook.type=2 GROUP BY number`, [fromdate, todate, body.employeeId]);
    
    let data2 = [];
    let i, j;
    for(i = 0; i<data.length; i++){
        for(j = 0; j<data1.length; j++){
            if(data[i].number === data1[j].number){
                if(Math.floor(data[i].incomingCall)>Math.floor(data1[j].outgoingCall)){
                    data[i].outgoingCall = data1[j].outgoingCall;
                    data2.push(data[i]);
                }
            }
        }
    }
    
    for(i = 0; i<data2.length; i++){
        data2[i].factor = Math.floor(data2[i].incomingCall)/util.max(data2[i].outgoingCall, 1);
    }
    // sort
    
    let res = [];
    let curItem;
    while(res.length<data2.length){
        curItem = {factor:0};
        let maxIndex=0;
        for(i = 0; i<data2.length; i++){
            if(data2[i].factor>curItem.factor){
                curItem = data2[i];
                maxIndex = i;
            }
        }
        data2[maxIndex].factor = 0;

        let contactInfo = await pool.query("select name from contact where employee_id=? and number=? limit 0,1", [body.employeeId, curItem.number]);
        curItem.name = contactInfo[0]?contactInfo[0].name:undefined;
        res.push(curItem);
    }
    
    return (res);
}

// average calls
async function Averages(body, fromdate, todate, limitFrom) {
    let data, data1;
    if(body.subType == 1){
        data = await pool.query(`select logbook.number,avg(logbook.duration) as incomingtotalduration
        from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=1)
        group by number
        order by incomingtotalduration desc limit ?, 10`, [fromdate, todate, body.employeeId, limitFrom]);
        
        for(let i = 0; i<data.length; i++){
            data1 = await Promise.all([
                pool.query(`select avg(logbook.duration) as outgoingtalduration from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=2) and logbook.number=?`, [fromdate, todate, body.employeeId, data[i].number]),
                pool.query('select contact.number, contact.name, contact.photo from contact where contact.number=? and contact.employee_id=? group by number', [data[i].number, body.employeeId])
            ]);
            data[i].incomingtotalduration = util.getTimeStamp(data[i].incomingtotalduration)
            data[i].outgoingtalduration = util.getTimeStamp(data1[0][0] ? data1[0][0].outgoingtalduration : 0);
            data[i].name = data1[1][0] ? data1[1][0].name : undefined;
            data[i].photo = data1[1][0] ? data1[1][0].photo : undefined;
        }
    }else{
        data = await pool.query(`select logbook.number,avg(logbook.duration) as outgoingtalduration
        from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=2)
        group by number
        order by outgoingtalduration desc limit ?, 10`, [fromdate, todate, body.employeeId, limitFrom]);
        
        for(let i = 0; i<data.length; i++){
            data1 = await Promise.all([
                pool.query(`select avg(logbook.duration) as incomingtotalduration from logbook where (date BETWEEN ? and ?) and  logbook.employee_id =? and (logbook.type=1) and logbook.number=?`, [fromdate, todate, body.employeeId, data[i].number]),
                pool.query('select contact.number, contact.name, contact.photo from contact where contact.number=? and contact.employee_id=? group by number', [data[i].number, body.employeeId])
            ]);
            data[i].outgoingtalduration = util.getTimeStamp(data[i].outgoingtalduration)
            data[i].incomingtotalduration = util.getTimeStamp(data1[0][0] ? data1[0][0].incomingtotalduration : 0);
            data[i].name = data1[1][0] ? data1[1][0].name : undefined;
            data[i].photo = data1[1][0] ? data1[1][0].photo : undefined;
        }
    }
    
    return data;
}

// weekly dashboard
async function weekdashboard(body) {
    let today = new Date();
    let monday = util.getMonday(today);
    monday = monday.getTime();
    today = today.getTime();
    let days = ["SUNDAY", "MONDAY", "TUESDAY", "THURSDAY", "FRIDAY", "SATURDAY"];
    let data_incoming = await pool.query(`select logbook.number, count(logbook.duration) as count, contact.name, contact.photo
    from logbook 
    LEFT JOIN contact on logbook.number = contact.number
    where (date BETWEEN ? and ?) and logbook.employee_id =? and logbook.type=1
    group by duration
    order by count desc limit 0,3`, [monday, today, body.employeeId]);
    let data_outgoing = await pool.query(`select logbook.number, count(logbook.duration) as count, contact.name, contact.photo
    from logbook 
    LEFT JOIN contact on logbook.number = contact.number
    where (date BETWEEN ? and ?) and logbook.employee_id =? and logbook.type=2
    group by duration
    order by count desc limit 0,3`, [monday, today, body.employeeId]);
    let data_most_duration = await pool.query(`select logbook.number, sum(logbook.duration) as totalduration, contact.name, contact.photo
    from logbook 
    LEFT JOIN contact on logbook.number = contact.number
    where (date BETWEEN ? and ?) and logbook.employee_id =?
    group by duration
    order by totalduration desc limit 0,3`, [monday, today, body.employeeId]);
    for (let i = 0; i < data_most_duration.length; i++) {
        data_most_duration[i].totalduration = util.getTimeStamp(data_most_duration[i].totalduration)
    }

    let data_most_calls = await pool.query(`select logbook.number, logbook.duration as duration, contact.name, contact.photo,logbook.type,logbook.date
    from logbook 
    LEFT JOIN contact on logbook.number = contact.number
    where (date BETWEEN ? and ?) and logbook.employee_id =?
    order by duration desc limit 0,3`, [monday, today, body.employeeId]);
    for (let i = 0; i < data_most_calls.length; i++) {
        data_most_calls[i].duration = util.getTimeStamp(data_most_calls[i].duration)
        data_most_calls[i].day = days[new Date(data_most_calls[i].date).getDay()];
    }

    let data_forgotten = await pool.query(`select logbook.number, contact.name, contact.photo,logbook.date
    from logbook 
    LEFT JOIN contact on logbook.number = contact.number
    where (date BETWEEN ? and ?) and logbook.employee_id =? and logbook.type=3
    order by date desc limit 0,3`, [monday, today, body.employeeId]);
    for (let i = 0; i < data_forgotten.length; i++) {
        data_forgotten[i].day = days[new Date(data_forgotten[i].date).getDay()];
        let date = new Date(data_forgotten[i].date);
        data_forgotten[i].time = date.toLocaleTimeString()
    }
    return {
        incoming: data_incoming,
        outgoing: data_outgoing,
        most_duration: data_most_duration,
        most_calls: data_most_calls,
        most_forgot: data_forgotten
    }
}
