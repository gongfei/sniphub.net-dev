var bcrypt = require('bcrypt');
var pool = require('../database');
var _CONFIG = require('../config');
var nodemailer = require('nodemailer');
let util = require('../global/utilit');
const request = require('request');

const saltRounds = 10;

exports.index = function (req, res, next) {
    res.send({
        message: "starting"
    })
}

exports.login = async function (req, res, next) {
    try {
        var password_hash = req.body.password;

        let admins = await pool.query('select * from admin where username=?', req.body.email);
        let admin = admins[0];
        if(admin && admin.username == req.body.email){
            let adminpassword = admin.password.toString();
            adminpassword = adminpassword.replace(/^\$2y(.+)$/i, '$2a$1')
            let matchpassword = await bcrypt.compare(password_hash, adminpassword);

            if (!(matchpassword === true)) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                return;
            }else{
                res.json({
                        success: true,
                        user: admin,
                        isAdmin: true
                    });
                return;
            }
        }
        let user = await pool.query('select * from employer where username=?', req.body.email);
        if (!(user && user.length > 0)) {
            res.json({ success: false, message: 'Authentication failed. User not found.' });
        } else if (user && user.length > 0) {
            // encrpted password compare
            let password = user[0].password_hash.toString();
            password = password.replace(/^\$2y(.+)$/i, '$2a$1')
            let matchpassword = await bcrypt.compare(password_hash, password);

            if (!(matchpassword === true)) {
                res.json({ success: false, message: 'Authentication failed. Wrong password.' });
                return;
            } else {
                 res.json({
                        success: true,
                        user: user[0],
                        isAdmin: false
                    });
            }

        }
    } catch (e) {
        res.send({
            success: false,
            message: e.message
        })
    }

}

exports.changePassword = async function (req, res) {
     try {
        let curPassword = req.body.cur_password;
        let newPassword = req.body.new_password;
        
        let username = req.body.username;
        let new_password_hash = "";
        await bcrypt.hash(newPassword, saltRounds, function (err, hash) {
            // Store hash in database
            new_password_hash = hash;
        });

        let user = await pool.query("select * from employer where username=?", [username]);
        if (user.length === 0) {
            res.send({
                success: false,
                message: "Current User is not Exist."
            });
            return;
        } else {
            
            let pwd_hash = user[0].password_hash.toString();
            pwd_hash = pwd_hash.replace(/^\$2y(.+)$/i, '$2a$1')
            let matchpassword = await bcrypt.compare(curPassword, pwd_hash);
            if (!(matchpassword === true)) {
                res.json({ success: false, message: 'Current Password is Wrong.' });
                return;
            }else{
                await pool.query("UPDATE employer SET password_hash=? where username=?", [new_password_hash, username]);
                res.send({
                    success: true,
                    message: "Password changed successfully."
                })
            }
        }

    } catch (e) {
        res.send({
            success: false,
            message: e.message
        })
    }
}


// change email
exports.changeEmail = async function(req, res){
    try {
        let curEmail = req.body.cur_email;
        let newEmail = req.body.new_email;
      
        let user = await pool.query("select * from employer where username=?", [curEmail]);
        if (user.length === 0) {
            res.send({
                success: false,
                message: "Current Email is not valid."
            })
        } else {
            let userId = user[0].id;
            await pool.query("UPDATE employer SET username=? where id=?", [newEmail, userId]);
            res.send({
                success: true,
                message: "Email changed successfully."
            })
        }

    } catch (e) {
        res.send({
            success: false,
            message: e.message
        })
    }
}

// email verify
exports.emailVerify = async function(req, res){
    try{
        let email = req.body.email;
        let returnUrl = req.body.returnUrl;
        let user = await pool.query("select * from employer where username=?", [email]);

        if (user.length === 0) {
            res.send({
                success: false,
                message: "Current Email is not valid."
            });
            return;
        }

        let supports = await pool.query("select * from support");
        let support = supports[0];
        
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            // auth: {
            //     user: _CONFIG.authmail.user,
            //     pass: _CONFIG.authmail.pass,
            // }
            auth: {
                user: support.email,
                pass: support.password
            }
        });

        let token = util.makeToken();

        let token_hash = "";
        await bcrypt.hash(token, saltRounds, function (err, hash) {
            // Store hash in database
            token_hash = hash;
            pool.query("UPDATE employer SET token=? where username=?", [token_hash, email]);
        });
        
        let endOfLine = require('os').EOL;
        let mailOptions = {
            from: support.email,
            to: email,
            subject: "CallMonitor Password Reset",
            html: "<p>Please click below link to reset your password</p><p>"+ returnUrl+"?token="+token +"</p>"
        }

        transporter.sendMail(mailOptions, function(error, info){
            if(error){

                res.send({
                    success: false,
                    message: error.message
                });
                return;
            }else{
                res.send({
                    success: true,
                    message: info.response
                })
            }
        });

        
        
    }catch (e){
        res.send({
            success: false,
            message: e.message
        })
    }
}


// reset password
exports.resetPassword = async function(req, res){
    try{
        let email = req.body.email;
        let token = req.body.token;
        let new_password = req.body.new_password;

        let user = await pool.query("select * from employer where username=?", [email]);
        if (user.length === 0) {
            res.send({
                success: false,
                message: "Current Email is not valid."
            });
            return;
        }else{
            if(user[0].token){
                let token_hash = user[0].token.toString();
                token_hash = token_hash.replace(/^\$2y(.+)$/i, '$2a$1')
                let matchtoken = await bcrypt.compare(token, token_hash);
                if(!matchtoken){
                    res.send({
                        success: false,
                        message: "Token mismatch"
                    });
                    return;
                }
            }
        }
        
        
        let new_password_hash = "";
        await bcrypt.hash(new_password, saltRounds, function (err, hash) {
            // Store hash in database
            new_password_hash = hash;
            pool.query("UPDATE employer SET password_hash=?,token=? where username=?", [new_password_hash, '', email]);
        });

        user = await pool.query("select * from employer where username=?", [email]);

        if (user[0].username === _CONFIG.admin.email) {
                res.json({
                    success: true,
                    user: user[0],
                    isAdmin: true
                });
            } else {
                res.json({
                    success: true,
                    user: user[0],
                    isAdmin: false
                });
            }
        return;
        
    }catch (e){
        res.send({
            success: false,
            message: e.message
        })
    }
}