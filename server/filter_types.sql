/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100132
Source Host           : localhost:3306
Source Database       : caminho

Target Server Type    : MYSQL
Target Server Version : 100132
File Encoding         : 65001

Date: 2018-09-24 11:40:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for filter_types
-- ----------------------------
DROP TABLE IF EXISTS `filter_types`;
CREATE TABLE `filter_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('article','book','action','socialize') COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of filter_types
-- ----------------------------
INSERT INTO `filter_types` VALUES ('1', 'article', 'political_science', 'Political Science');
INSERT INTO `filter_types` VALUES ('2', 'article', 'psychology', 'Psychology');
INSERT INTO `filter_types` VALUES ('3', 'article', 'economy', 'Economy');
INSERT INTO `filter_types` VALUES ('4', 'article', 'history', 'History');
INSERT INTO `filter_types` VALUES ('5', 'action', 'planning', 'Planning');
INSERT INTO `filter_types` VALUES ('6', 'action', 'tools', 'Tools');
INSERT INTO `filter_types` VALUES ('8', 'article', 'lessons', 'Lessons');
INSERT INTO `filter_types` VALUES ('9', 'book', 'science', 'Science');
INSERT INTO `filter_types` VALUES ('10', 'book', 'physical', 'Physical');
INSERT INTO `filter_types` VALUES ('11', 'book', 'economy', 'Economy');
INSERT INTO `filter_types` VALUES ('12', 'book', 'history', 'History');
INSERT INTO `filter_types` VALUES ('13', 'action', 'planning', 'Planning');
INSERT INTO `filter_types` VALUES ('14', 'action', 'tools', 'Tools');
INSERT INTO `filter_types` VALUES ('15', 'socialize', 'facebook_groups', 'Facebook Groups');
INSERT INTO `filter_types` VALUES ('16', 'socialize', 'meetup_groups', 'Meetup Groups');
INSERT INTO `filter_types` VALUES ('17', 'socialize', 'other blogs', 'Other Blogs');
